//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//#include <cmath>
#include <iostream>
#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/HEPEVT_Wrapper.h"
#include "PythiaHelper.h"

using namespace std;

extern "C" {   
    // Instantiate an IO strategy for reading from HEPEVT.
       HepMC::IO_HEPEVT hepevtio;
    // Instantial an IO strategy to write the data to file - it uses the 
    //  same ParticleDataTable
    //   const char outfile[] = "/tmp/example_out.dat";
       char * outfile = getenv ("HEPMCOUT");
       int ncount = 0;   
       HepMC::IO_GenEvent ascii_io(outfile ,std::ios::out);
       //HepMC::IO_HEPEVT::set_trust_mothers_before_daughters( true );
    // open output file for eye readable ascii output
    // begin scope of ascii_io 
  //   HepMC::IO_AsciiParticles ascii_io("readable_eventlist.dat",std::ios::out);
    // create an empty GenCrossSection object
       HepMC::GenCrossSection cross;

//void convhepmc_(int & ievent, int & iproc, double & xsec){
void convhepmc_(int & ievent, int & iproc, double & xsec, double & xsece ){
      
      char * pPath;
      pPath = getenv ("HEPMCOUT");
      if ( ncount < 10) {
        if (pPath!=NULL) { cout << " env variable = " <<  pPath << endl;}
          else { cout << " NO HEPMCOUT environment varibale set " <<endl; 
          return ;}
          ++ncount;
      }

   // pythia pyhepc routine convert common PYJETS in common HEPEVT
	call_pyhepc( 1 );
      HepMC::GenEvent* evt = hepevtio.read_next_event();
      //HepMC::IO_HEPEVT::print_inconsistency_errors();
      
	// HepMC::HEPEVT_Wrapper::check_hepevt_consistency();
      // HepMC::IO_HEPEVT::set_trust_mothers_before_daughters( true );
      //   from version 2.06.09 on:      
      //      evt->define_units( HepMC::Units::GEV, HepMC::Units::MM );
      evt->use_units(HepMC::Units::GEV, HepMC::Units::MM);
      evt->set_event_number(ievent);
      evt->set_signal_process_id(iproc);
      //      std::cout << " ievent " << ievent << " iproc " << iproc << " xsec " <<xsec<< std::endl;
      // set cross section information set_cross_sectio( xsec, xsec_err)
      const double xsecval = xsec;
      const double xsecerr = xsece ;
      cross.set_cross_section( xsecval, xsecerr );
	evt->set_cross_section( cross );
      // write the event out to the ascii file
	ascii_io << evt;

    delete evt;
   }
}
