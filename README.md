# RAPGAP
RAPGAP is an MC generator which can be used to generate both DIS and Diffractive e+p events.
- Manual: [rapgap.pdf](http://projects.hepforge.org/rapgap/rapgap.pdf)
- www-page: [http://projects.hepforge.org/rapgap/]
- RAPGAP has radiative corrections included. The program used is HERACLES. Info can be found [here](http://www.desy.de/~hspiesb/heracles.html)
- [Here](https://wiki.bnl.gov/eic/index.php/RAPGAP_compile) are some notes how to compile rapgap-32 for 32-bit and 64bit on the racf-cluster.

# Installation

Instructions on RCF, adapt to your LHAPDF, PYTHIA, etc.
```
  ## need to regenerate autoconf files
  aclocal ; autoconf ; automake --add-missing
  ./configure --disable-shared --prefix=$EICDIRECTORY --with-pythia6=$PYTHIA6  --with-lhapdf=$LHAPDF5
  make -j 8 install
```
    
# Running
/path/to/rapgap33 < path/to/share/steer-ep 


# Known Issues
  - Standard instructions may not work, please consider
[https://wiki.bnl.gov/eic/index.php/RAPGAP_compile]

  - Do NOT use 3.303. The changes made to work with LHAPDF6 seem to make using LHAPDF5 impossible



# CHANGES to default package
- adapted two custom files for BNL output: src/rapgap/rgmain.F src/rapgap/analys.F
- in configure.ac remove spurious dummy:

```
AC_SUBST([PYTHIALIB], ["-L$with_pythia6/lib -lpythia6 "])
AC_MSG_RESULT([Using -L$with_pythia6/lib -lpythia6 ])
```


- Support non-standard library location

```
AC_MSG_CHECKING([for LHAPDF])
AC_ARG_WITH([lhapdf],[AS_HELP_STRING([--with-lhapdf],[path to LHAPDF installation])],[:],[with_lhapdf=${LHAPDF_DIR}])
AC_SUBST([PDFLIBPATH],`find $with_lhapdf  -maxdepth 2 -name libLHAPDF.\* | head -n 1`)
AS_IF( [test "x$PDFLIBPATH" == "x"] , [AC_MSG_ERROR([Cannot find libLHAPDF.* under $with_lhapdf
       Specify correct path to LHAPDF installation: --with-lhapdf=/path/to/lhapdf] )],
       [AC_SUBST([PDFLIB], ["-L`dirname $PDFLIBPATH` -lLHAPDF "])])
AC_MSG_RESULT([Using $PDFLIB ])
```

- A minor change to Makefile.am:

```
if BUILD_DOCS
   MANUAL_OPT = manual
else
   MANUAL_OPT = 
endif
SUBDIRS = include misc bases51  $(ARIADNE_OPT) src $(MAYBE_OPT) data $(MANUAL_OPT)
```

